= Pry debugger

link:index.adoc[]

.MyTitle
[#my_id]
pry::some_target[#some_id, some_attr, some_key=other_attr]

- link:test.adoc[#some_id, some_attr, some_key=other_attr]
- link:test.adoc["#some_id, some_attr, some_key=other_attr"]
- link:test.adoc["#some_id, \"some_attr\", some_key=other_attr"]